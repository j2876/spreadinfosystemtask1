import org.apache.commons.cli.*;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.AbstractMap.SimpleEntry;
import java.util.logging.XMLFormatter;

import static java.util.Comparator.comparing;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class.getName());

    public static void main(String[] args) throws ParseException, IOException {
        logger.log(Level.INFO, "Task 1");
        Options options = new Options();
        options.addOption("f", "file", true, "Input xml file");
        options.addOption("l", "limit", true, "Limit number of XML elements");
        final var parser = new DefaultParser();
        var commandLine = parser.parse(options, args);

        int limit = Integer.parseInt(commandLine.getOptionValue('l', "1000"));
        var filePath = commandLine.getOptionValue('f');
        int counter = 0;

        try (CompressorInputStream in = new BZip2CompressorInputStream(
                new BufferedInputStream(new FileInputStream(filePath), 4096 * 22)
        )) {
            HashMap<String, HashSet<String>> userToChangeSet = new HashMap<>();

            XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
            XMLEventReader reader = xmlInputFactory.createXMLEventReader(in);
            var user = new QName("user");
            var changeSet = new QName("changeset");
            var parentElements = new ArrayDeque<StartElement>();
            var uniqTags = new HashSet<String>();

            var tagToNodeCountMap = new HashMap<String, Integer>();

            while (reader.hasNext()) {
                XMLEvent nextEvent = reader.nextEvent();
                if (limit-- <= 0) break;
                if (nextEvent.isStartElement()) {
                    StartElement startElement = nextEvent.asStartElement();
                    var eventName = startElement.getName().getLocalPart();
                    var aUser = startElement.getAttributeByName(user);
                    if (aUser != null) {
                        String userStr = aUser.getValue();
                        String changeSetStr = startElement.getAttributeByName(changeSet).getValue();
                        userToChangeSet.putIfAbsent(userStr, new HashSet<>());
                        userToChangeSet.get(userStr).add(changeSetStr);
                    }

                    if ("tag".equals(eventName)) {
                        if (parentElements.getLast().getName().getLocalPart().equals("node")) {
                            String tagKey = nextEvent.asStartElement().getAttributeByName(new QName("k")).getValue();
                            uniqTags.add(tagKey);
                            tagToNodeCountMap.merge(tagKey, 1, Integer::sum);
                        }
                    }

                    parentElements.addLast(startElement);
                }

                if (nextEvent.isEndElement()) {
                    parentElements.removeLast();
                }
            }

            System.out.println(counter);

            userToChangeSet.entrySet().stream().map(entry -> new SimpleEntry<>(entry.getKey(), entry.getValue().size()))
                    .sorted(Comparator.<SimpleEntry<String, Integer>>comparingInt(SimpleEntry::getValue).reversed())
                    .forEach(pair -> System.out.println(pair.getKey() + " " + pair.getValue()));

            System.out.println("---------------------------------------");
            System.out.println("Count of uniq tags: " + uniqTags.size());
            System.out.println("");
            tagToNodeCountMap
                    .entrySet().stream().map(entry -> new SimpleEntry<>(entry.getKey(), entry.getValue()))
                    .sorted(Map.Entry.comparingByValue())
                    .forEach(key -> {
                System.out.println(key + " : " + tagToNodeCountMap.get(key.getKey()));
            });

        } catch (XMLStreamException e) {
            e.printStackTrace();
        }


    }
}
